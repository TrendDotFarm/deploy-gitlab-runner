# Deploy GitLab Runner

To execute the playbook, run

```sh
ansible-playbook --inventory <target_host>, gitlab-runner-playbook.yml
```

Don't forget the comma! `<target_host>` can be an IP address or a
domain.
